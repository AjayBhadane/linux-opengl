#include <stdio.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

char * getTextFromFile(const char* path){
    FILE *f = fopen(path, "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */

    char *string = (char *)malloc(fsize + 1);
    fread(string, 1, fsize, f);
    fclose(f);

    string[fsize] = 0;

    return string;
}

int main(int argc, char** argv){
    const char* path = "src/test";

    std::cout << getTextFromFile(path) << std::endl;
}